#Welcome to aditas-invoice

## Start the application with Maven

```bash
$> ./mvnw -Dspring-boot.run.profiles=dev spring-boot:run # DEV (no Syrius call)
$> ./mvnw spring-boot:run # PROD
```

## Start the application as .jar

```bash
$> java -jar <JAR> --spring.profiles.active=dev # DEV (no Syrius call)
$> java -jar <JAR> # PROD
```

## Access application

Go to http://localhost:8888/kundenportal/invoice

## Syrius Notification Endpoint

Get the WSDL: http://localhost:8888/kundenportal/ws/syriusNotification.wsdl

Call the endpoint: http://localhost:8888/kundenportal/ws

If you want to make a test request, just start the application and cd to src/test/resources:
```bash
$> curl -X POST -H "Content-Type: text/xml" -d @syrius-client-test-notification.xml http://localhost:8888/kundenportal/ws
```

## "Helper" methods

Send request to Syrius createInvoice endpoint (does NOT work in dev profile):
```bash
$> curl http://localhost:8888/kundenportal/syriushelper/addInvoice/<ID>
```

Save a new invoice to the database:
```bash
$> curl http://localhost:8888/kundenportal/h2helper/save # ID is returned
```

Get information to specific invoice:
```bash
$> curl http://localhost:8888/kundenportal/h2helper/read/<ID>
```

Update invoice (Websocket topic is called):
```bash
$> http://localhost:8888/kundenportal/h2helper/update/2/<STATUS>
# Status may be: EMPFANGEN, IN_BEARBEITUNG, FREIGEGEBEN
```