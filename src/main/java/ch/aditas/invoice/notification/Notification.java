package ch.aditas.invoice.notification;

public class Notification {

    private long rechnungId;

    private String status;

    public Notification(long rechnungId, String status) {
        this.rechnungId = rechnungId;
        this.status = status;
    }

    public long getRechnungId() {
        return rechnungId;
    }

    public void setRechnungId(long rechnungId) {
        this.rechnungId = rechnungId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
