package ch.aditas.invoice.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class NotificationController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

    @MessageMapping("/sendPush")
    @SendTo("/topic/notifications")
    public Notification updateClient() {
        LOGGER.info("Send new notification ...");
        return new Notification(1, "GESPEICHERT");
    }

    @GetMapping("/notification/test")
    @ResponseBody
    public void testNotification() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String jsonNotification = mapper.writeValueAsString(new Notification(1, "FREIGEGEBEN"));
        simpMessagingTemplate.convertAndSend("/topic/notifications",jsonNotification);
    }
}
