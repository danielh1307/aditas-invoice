package ch.aditas.invoice;

public enum InvoiceStatus {

    EMPFANGEN,
    IN_BEARBEITUNG,
    FREIGEGEBEN
}
