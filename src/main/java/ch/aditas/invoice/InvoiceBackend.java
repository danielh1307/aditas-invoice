package ch.aditas.invoice;

public interface InvoiceBackend {

    void createInvoice(String invoiceId) throws Exception;
}
