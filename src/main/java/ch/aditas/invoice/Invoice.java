package ch.aditas.invoice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Invoice {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private InvoiceStatus status;
    private String description;

    public Invoice() {
        // needed by JPA
    }

    public Invoice(String description, InvoiceStatus status) {
        this.description = description;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id.toString() + " " + description + " " + status;
    }
}
