package ch.aditas.invoice.infra;

import ch.aditas.invoice.Invoice;
import ch.aditas.invoice.InvoiceRepository;
import ch.aditas.invoice.InvoiceStatus;
import ch.aditas.invoice.notification.Notification;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

import static ch.aditas.invoice.InvoiceStatus.EMPFANGEN;

@Controller
@RequestMapping("/h2helper")
public class H2HelperController {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("/save")
    @ResponseBody
    private String h2Save() {
        Invoice save = invoiceRepository.save(new Invoice("Beschreibung", EMPFANGEN));
        return save.getId().toString();
    }

    @GetMapping("/read/{id}")
    @ResponseBody
    private String h2Read(@PathVariable String id) {
        Optional<Invoice> byId = invoiceRepository.findById(Long.valueOf(id));
        return byId.toString();
    }

    @GetMapping("/update/{id}/{status}")
    @ResponseBody
    private String h2Update(@PathVariable String id, @PathVariable String status) throws Exception {
        Invoice byId = invoiceRepository.findById(Long.valueOf(id)).get();
        byId.setStatus(InvoiceStatus.valueOf(status));
        invoiceRepository.save(byId);
        updateWebsocket(byId);
        return "success";
    }

    private void updateWebsocket(Invoice invoice) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String jsonNotification = mapper.writeValueAsString(new Notification(invoice.getId(), invoice.getStatus().name()));
        simpMessagingTemplate.convertAndSend("/topic/notifications",jsonNotification);
    }

}
