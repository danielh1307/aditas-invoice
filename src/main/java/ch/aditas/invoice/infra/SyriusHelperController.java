package ch.aditas.invoice.infra;

import ch.aditas.invoice.InvoiceBackend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/syriushelper")
public class SyriusHelperController {

    @Autowired
    private InvoiceBackend invoiceBackend;

    @GetMapping("/addInvoice/{invoiceId}")
    @ResponseBody
    public void syrius(@PathVariable String invoiceId) throws Exception {
        invoiceBackend.createInvoice(invoiceId);
    }
}
