package ch.aditas.invoice.infra;

public class SyriusRequests {

    public static final String SYRIUS_REQUEST = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:syrius.modul_bl.service.soap\" xmlns:ecl=\"http://www.adcubum.com/wsdl/syrius.ch.eclaim.modul_bl.eclaim.kvuv.ws_service/EClaimInvoiceWSAPI\">\n" +
            "    <soapenv:Header>\n" +
            "        <urn:SoapLoginInformation>\n" +
            "            <Password>Cube12@AD!</Password>\n" +
            "            <Username>SyriusAdmin</Username>\n" +
            "        </urn:SoapLoginInformation>\n" +
            "    </soapenv:Header>\n" +
            "    <soapenv:Body>\n" +
            "        <ecl:createInvoice>\n" +
            "            <ecl:rechnungId>TODO-ID</ecl:rechnungId>\n" +
            "            <ecl:xml>TODO-XML</ecl:xml>\n" +
            "        </ecl:createInvoice>\n" +
            "    </soapenv:Body>\n" +
            "</soapenv:Envelope>";

    public static final String SYRIUS_REQUEST_CONTENT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
            "<request language=\"de\" modus=\"production\" validation_status=\"0\" xsi:schemaLocation=\"http://www.forum-datenaustausch.ch/invoice generalInvoiceRequest_440.xsd\" xmlns=\"http://www.forum-datenaustausch.ch/invoice\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "    <processing print_at_intermediate=\"false\" print_patient_copy=\"false\">\n" +
            "        <transport from=\"7601002001540\" to=\"7601003006361\">\n" +
            "            <via via=\"7601001304307\" sequence_id=\"1\"/>\n" +
            "        </transport>\n" +
            "    </processing>\n" +
            "    <payload type=\"invoice\" storno=\"false\" copy=\"false\">\n" +
            "        <invoice request_timestamp=\"1529329226\" request_date=\"2016-08-08T00:00:00\" request_id=\"S_000772_004817_G\"/>\n" +
            "        <body role=\"physician\" place=\"practice\">\n" +
            "            <prolog>\n" +
            "                <generator name=\"XSLT-Transformation\" version=\"0\" id=\"0\"/>\n" +
            "            </prolog>\n" +
            "            <balance currency=\"CHF\" amount=\"1155.950\" amount_reminder=\"0\" amount_prepaid=\"0\" amount_due=\"1155.950\" amount_obligations=\"1155.950\">\n" +
            "                <vat vat_number=\"0\" vat=\"1155.950\">\n" +
            "                    <vat_rate vat_rate=\"0\" amount=\"99\" vat=\"0\"/>\n" +
            "                </vat>\n" +
            "            </balance>\n" +
            "            <esr9 type=\"16or27\" participant_number=\"01-1067-4\" reference_number=\"00 00000 00000 00000 00000 00000\" coding_line=\"0100000099008&gt;000000000000000000000000000+ 010010674&gt;\"/>\n" +
            "            <tiers_garant payment_period=\"P30D\">\n" +
            "                <biller zsr=\"I184801\" specialty=\"Ernährungsberatung\">\n" +
            "                    <person salutation=\"Herr\">\n" +
            "                        <familyname>Hochstrasser-Pfrunder</familyname>\n" +
            "                        <givenname>Tina</givenname>\n" +
            "                        <postal>\n" +
            "                            <street>Waldheimstr.</street>\n" +
            "                            <zip countrycode=\"CH\">6314</zip>\n" +
            "                            <city>Unterägeri</city>\n" +
            "                        </postal>\n" +
            "                    </person>\n" +
            "                </biller>\n" +
            "                <provider zsr=\"I184801\" specialty=\"Ernährungsberatung\">\n" +
            "                    <person salutation=\"Herr\">\n" +
            "                        <familyname>Hochstrasser-Pfrunder</familyname>\n" +
            "                        <givenname>Tina</givenname>\n" +
            "                        <postal>\n" +
            "                            <street>Waldheimstr.</street>\n" +
            "                            <zip countrycode=\"CH\">6314</zip>\n" +
            "                            <city>Unterägeri</city>\n" +
            "                        </postal>\n" +
            "                    </person>\n" +
            "                </provider>\n" +
            "                <insurance ean_party=\"7601003006361\">\n" +
            "                    <company>\n" +
            "                        <companyname>Default OE-Direktion</companyname>\n" +
            "                        <department>Natürliche- oder juristische Person</department>\n" +
            "                        <postal>\n" +
            "                            <zip countrycode=\"CH\">9000</zip>\n" +
            "                            <city>St. Gallen</city>\n" +
            "                        </postal>\n" +
            "                    </company>\n" +
            "                </insurance>\n" +
            "                <patient gender=\"male\" birthdate=\"1980-01-01T00:00:00\">\n" +
            "                    <person salutation=\"Herr\">\n" +
            "                        <familyname>Grolimund</familyname>\n" +
            "                        <givenname>Urs</givenname>\n" +
            "                        <postal>\n" +
            "                            <street>St.Leonhardstrasse 40</street>\n" +
            "                            <zip countrycode=\"CH\">9001</zip>\n" +
            "                            <city>St. Gallen</city>\n" +
            "                        </postal>\n" +
            "                    </person>\n" +
            "                </patient>\n" +
            "                <insured gender=\"male\" birthdate=\"1980-01-01T00:00:00\">\n" +
            "                    <person salutation=\"Herr\">\n" +
            "                        <familyname>Grolimund</familyname>\n" +
            "                        <givenname>Urs</givenname>\n" +
            "                        <postal>\n" +
            "                            <street>St.Leonhardstrasse 40</street>\n" +
            "                            <zip countrycode=\"CH\">9001</zip>\n" +
            "                            <city>St. Gallen</city>\n" +
            "                        </postal>\n" +
            "                    </person>\n" +
            "                </insured>\n" +
            "                <guarantor>\n" +
            "                    <person salutation=\"Herr\">\n" +
            "                        <familyname>Grolimund</familyname>\n" +
            "                        <givenname>Urs</givenname>\n" +
            "                        <postal>\n" +
            "                            <street>St.Leonhardstrasse 40</street>\n" +
            "                            <zip countrycode=\"CH\">9001</zip>\n" +
            "                            <city>St. Gallen</city>\n" +
            "                        </postal>\n" +
            "                    </person>\n" +
            "                </guarantor>\n" +
            "                <referrer ean_party=\"7601000269652\" zsr=\"K042017\" specialty=\"Allgemeine Innere Medizin\">\n" +
            "                    <person salutation=\"Herr\">\n" +
            "                        <familyname>Sonderegger</familyname>\n" +
            "                        <givenname>Thomas</givenname>\n" +
            "                        <postal>\n" +
            "                            <street>Mövenstr.</street>\n" +
            "                            <zip countrycode=\"CH\">9015</zip>\n" +
            "                            <city>St. Gallen</city>\n" +
            "                        </postal>\n" +
            "                    </person>\n" +
            "                </referrer>\n" +
            "            </tiers_garant>\n" +
            "            <kvg insured_id=\"PARKVG08_01290\"/>\n" +
            "            <treatment date_begin=\"2016-08-04T00:00:00\" date_end=\"2016-08-04T00:00:00\" canton=\"ZG\" reason=\"disease\" apid=\"PARKVG02_05299\">\n" +
            "                <diagnosis type=\"freetext\">grippaler Infekt</diagnosis>\n" +
            "                <xtra_hospital>\n" +
            "                    <ambulatory hospitalization_type=\"regular\" class=\"general\"/>\n" +
            "                </xtra_hospital>\n" +
            "            </treatment>\n" +
            "            <services>\n" +
            "                <record_tarmed tariff_type=\"001\" record_id=\"1\" code=\"00.1710\" name=\"Akupunktur, Konsultation durch den Facharzt, erste 5 Min.\" session=\"1\" quantity=\"50\" date_begin=\"2016-08-04T00:00:00\" date_end=\"2016-08-04T00:00:00\" provider_id=\"0000000000000\" responsible_id=\"0000000000000\" unit=\"888.000\" unit_factor=\"1\" external_factor=\"1\" amount=\"888.000\" vat_rate=\"0\" validate=\"false\" obligation=\"true\"/>\n" +
            "                <record_drug tariff_type=\"400\" record_id=\"1\" code=\"1974508\" name=\"VIAGRA Filmtabl 100 mg 12 Stk\" quantity=\"1\" date_begin=\"2016-08-04T00:00:00\" date_end=\"2016-08-04T00:00:00\" provider_id=\"0000000000000\" responsible_id=\"0000000000000\" unit=\"267.95\" unit_factor=\"1\" external_factor=\"1\" amount=\"267.95\" vat_rate=\"0\" validate=\"false\" obligation=\"true\">\n" +
            "                    <xtra_drug indicated=\"true\" iocm_category=\"A\" delivery=\"first\" limitation=\"false\"/>\n" +
            "                </record_drug>\n" +
            "            </services>\n" +
            "        </body>\n" +
            "    </payload>\n" +
            "</request>\n";
}
