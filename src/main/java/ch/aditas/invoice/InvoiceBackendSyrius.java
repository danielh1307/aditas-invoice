package ch.aditas.invoice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

import static ch.aditas.invoice.infra.SyriusRequests.SYRIUS_REQUEST;
import static ch.aditas.invoice.infra.SyriusRequests.SYRIUS_REQUEST_CONTENT;

@Component
@Profile("!dev")
public class InvoiceBackendSyrius implements InvoiceBackend {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceBackendSyrius.class);

    @Override
    public void createInvoice(String invoiceId) throws Exception {
        String syriusRequestFileContent = SYRIUS_REQUEST;
        String syriusXmlFileContent = SYRIUS_REQUEST_CONTENT;

        String syriusXmlFileContentBase64 = new String(Base64.getEncoder().encodeToString(syriusXmlFileContent.getBytes()));
        syriusRequestFileContent = syriusRequestFileContent.replaceFirst("TODO-ID", invoiceId);
        syriusRequestFileContent = syriusRequestFileContent.replaceFirst("TODO-XML", syriusXmlFileContentBase64);

        LOGGER.info("Sending request to Syrius: " + syriusRequestFileContent);

        URL url = new URL(" http://localhost:5156/syrius/soap/EClaimInvoiceWSAPI_Service");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        try {
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "text/xml");

            DataOutputStream dos = new DataOutputStream(con.getOutputStream());
            dos.writeBytes(syriusRequestFileContent);
            dos.flush();
            dos.close();

            //Get Response
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            String responseString = response.toString();
            LOGGER.info("Getting response from Syrius: " + responseString);
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
    }
}
