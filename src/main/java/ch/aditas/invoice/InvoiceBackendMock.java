package ch.aditas.invoice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class InvoiceBackendMock implements InvoiceBackend {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceBackendMock.class);

    @Override
    public void createInvoice(String invoiceId) throws Exception {
            LOGGER.info("Calling nothing with invoiceId " + invoiceId);
    }
}
