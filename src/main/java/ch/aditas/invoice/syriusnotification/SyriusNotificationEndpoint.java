package ch.aditas.invoice.syriusnotification;

import ch.aditas.invoice.Invoice;
import ch.aditas.invoice.InvoiceRepository;
import ch.aditas.invoice.InvoiceStatus;
import ch.aditas.invoice.notification.Notification;
import com.adcubum.aditas.RequestStatus;
import com.adcubum.aditas.ResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import static ch.aditas.invoice.InvoiceStatus.FREIGEGEBEN;
import static ch.aditas.invoice.InvoiceStatus.IN_BEARBEITUNG;

@Endpoint
public class SyriusNotificationEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(SyriusNotificationEndpoint.class);

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @PayloadRoot(namespace = "http://www.adcubum.com/aditas", localPart = "requestStatus")
    @ResponsePayload
    public ResponseStatus sendStatus(@RequestPayload RequestStatus requestStatus) throws Exception {
        LOGGER.info("Got Syrius event: " + requestStatus.getRechnungsid() + ", " + requestStatus.getStatus());

        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setStatus3("SUCCESS");

        Notification notification = new Notification(Long.valueOf(requestStatus.getRechnungsid()), translateStatus(requestStatus.getStatus()));
        updateDatabase(notification);
        updateWebsocket(notification);

        return responseStatus;
    }

    private void updateDatabase(Notification notification) {
        Invoice byId = invoiceRepository.findById(notification.getRechnungId()).get();
        byId.setStatus(InvoiceStatus.valueOf(notification.getStatus()));
        invoiceRepository.save(byId);
    }

    private void updateWebsocket(Notification notification) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String jsonNotification = mapper.writeValueAsString(notification);
        simpMessagingTemplate.convertAndSend("/topic/notifications",jsonNotification);
    }

    private String translateStatus(String status) {
        switch (status) {
            case "In Bearbeitung":
                return IN_BEARBEITUNG.name();
            case "Freigegeben":
                return FREIGEGEBEN.name();
            default:
                return status;
        }
    }

}
