package ch.aditas.invoice;

public class InvoiceForm {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
