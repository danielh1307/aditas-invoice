package ch.aditas.invoice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import static ch.aditas.invoice.InvoiceStatus.EMPFANGEN;

@Controller
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private InvoiceBackend invoiceBackend;

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceController.class);

    @GetMapping
    public String getInvoiceView(Model model) {
        model.addAttribute("invoices", getAllInvoicesFromDatabase());
        return "invoice";
    }

    @PostMapping
    public String addInvoice(InvoiceForm invoiceForm, @RequestParam MultipartFile invoice, Model model) throws Exception {
        LOGGER.info("Invoice form received: " + invoiceForm.getDescription());
        LOGGER.info("Invoice content: " + invoice);

        Invoice newInvoice = new Invoice(invoiceForm.getDescription(), EMPFANGEN);
        Long idOfPersistedInvoice = saveInvoiceToDatabase(newInvoice);
        invoiceBackend.createInvoice(String.valueOf(idOfPersistedInvoice));

        model.addAttribute("invoices", getAllInvoicesFromDatabase());
        return "invoice";
    }

    private Long saveInvoiceToDatabase(Invoice newInvoice) {
        Invoice persistedInvoice = invoiceRepository.save(newInvoice);
        return persistedInvoice.getId();
    }

    private List<Invoice> getAllInvoicesFromDatabase() {
        Iterable<Invoice> allInvoiceIterator = invoiceRepository.findAll();
        List<Invoice> invoiceList = new ArrayList<>();
        allInvoiceIterator.forEach(invoiceList::add);
        return invoiceList;
    }
}
