/** Custom scripts **/

var stompClient = null;
const statusReceived = "EMPFANGEN";
const statusSaved = "IN_BEARBEITUNG";
const statusProcessed = "FREIGEGEBEN";


function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/kundenportal/notification-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/notifications', function (notification) {
            processResponse(JSON.parse(notification.body));
        });
    });
}

/**
 * This method identifies what element inside the invoice table has to be updated
 *
 * @param notificationBody
 */
function processResponse(notificationBody) {
    var invoiceStatus = $("#invoice_overview").find('#' + notificationBody['rechnungId']);
    var invoiceStatusParent = invoiceStatus.parent();
    var statusElement = invoiceStatus.siblings(":last");
    var status = notificationBody['status'];
    statusElement.html(status);

    switch (status) {
        case statusSaved:
            statusElement.removeClass("status-saved status-processed animation");
            statusElement.addClass("status-saved animation");
            break;

        case statusProcessed:
            statusElement.removeClass("status-saved status-processed animation");
            invoiceStatus.siblings(":last").addClass("status-processed animation");

            break;

        default:
            //We do nothing here!
            break;
    }
}

function updateStateFromContent() {
    var invoiceStatuses = $("#invoice_overview").find('.status');

    invoiceStatuses.each(function() {
        var stateFromContent = $(this).html().toUpperCase();

        switch (stateFromContent) {
            case statusSaved:
                $(this).removeClass("status-saved status-processed animation");
                $(this).addClass("status-saved");
                break;

            case statusProcessed:
                $(this).removeClass("status-saved status-processed animation");
                $(this).addClass("status-processed");
                break;

            default:
                //We do nothing here!
                break;
        }
    });
}

$(document).ready(function () {
    updateStateFromContent();
    connect();
});

$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);

    var file = $(this)[0].files[0].name;
    $('.file-upload-label').text(file);
});
