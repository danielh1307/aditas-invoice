package ch.aditas.invoice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Optional;

import static java.util.Collections.EMPTY_LIST;

@Configuration
@Profile("dev")
public class TestConfig {

    @Bean
    public InvoiceRepository invoiceRepository() {
        return new InvoiceRepository() {
            @Override
            public <S extends Invoice> S save(S s) {
                return s;
            }

            @Override
            public <S extends Invoice> Iterable<S> saveAll(Iterable<S> iterable) {
                return null;
            }

            @Override
            public Optional<Invoice> findById(Long aLong) {
                return Optional.empty();
            }

            @Override
            public boolean existsById(Long aLong) {
                return false;
            }

            @Override
            public Iterable<Invoice> findAll() {
                return EMPTY_LIST;
            }

            @Override
            public Iterable<Invoice> findAllById(Iterable<Long> iterable) {
                return null;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void deleteById(Long aLong) {

            }

            @Override
            public void delete(Invoice invoice) {

            }

            @Override
            public void deleteAll(Iterable<? extends Invoice> iterable) {

            }

            @Override
            public void deleteAll() {

            }
        };
    }

}
