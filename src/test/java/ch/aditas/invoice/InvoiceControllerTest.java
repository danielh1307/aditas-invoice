package ch.aditas.invoice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest
@ComponentScan
@ActiveProfiles("dev")
public class InvoiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getInvoiceViewReturnsCorrectView() throws Exception {
        this.mockMvc
                .perform(get("/invoice"))
                .andExpect(status().isOk())
                .andExpect(view().name("invoice"));
    }

    @Test
    public void addInvoiceReturnsCorrectView() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
                "invoice",
                "filename.txt",
                "text/plain",
                "something".getBytes());

        this.mockMvc
                .perform(multipart("/invoice")
                        .file(file))
                .andExpect(status().isOk())
                .andExpect(view().name("invoice"));
    }
}
