FROM openjdk:8-jdk-alpine

ENV APP_ROOT=/opt/app
ENV PATH=${APP_ROOT}:${PATH} HOME=${APP_ROOT}

RUN mkdir -p ${APP_ROOT}
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} ${APP_ROOT}/app.jar

RUN chmod -R u+x ${APP_ROOT} && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd

COPY run-java.sh ${APP_ROOT}/
RUN chmod 755 ${APP_ROOT}/run-java.sh

WORKDIR ${APP_ROOT}
USER 1001

ENV JAVA_APP_JAR=${APP_ROOT}/app.jar

CMD [ "/opt/app/run-java.sh" ]